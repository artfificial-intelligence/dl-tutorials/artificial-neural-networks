# 인공 신경망: 기본과 원리  <sup>[1](#footnote_1)</sup>

<font size="3">인공 신경망의 기본과 원리에 대해 알아본다.</font>

## 목차

1. [개요](./artificial-neural-networks.md#intro)
1. [인공 신경망이란?](./artificial-neural-networks.md#sec_02)
1. [인공 신경망은 어떻게 동작할까?](./artificial-neural-networks.md#sec_03)
1. [인공 신경망의 타입](./artificial-neural-networks.md#sec_04)
1. [인공 신경망의 응용](./artificial-neural-networks.md#sec_05)
1. [인공 신경망의 과제와 한계](./artificial-neural-networks.md#sec_05)
1. [요약](./artificial-neural-networks.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 2 — Artificial Neural Networks: Basics and Principles](https://medium.com/ai-in-plain-english/dl-tutorial-2-artificial-neural-networks-basics-and-principles-40355d1331c1?sk=a496ca56a528786a5cd58d55dc90142f)를 편역하였습니다.
